<?php

/**
 * @package ccGeoRedirect
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    06 Sep 2017
 */

class ccGeoRedirect {

    /**
     * @var eZINI
     */
    protected $ini = null;

    /**
     * @var eZHTTPTool
     */
    protected $http = null;

    /**
     * Cookie variable which stores current user's country code
     *
     * @var string
     */
    protected static $countryCodeCookieVar = 'cc_geo_country';

    /**
     * Path to Geo IP database
     *
     * @var string
     */
    protected static $geoDatabasePath = 'extension/cc_georedirect/lib/geoip/GeoIP.dat';


    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        $this->ini  = eZINI::instance('georedirect.ini');
        $this->http = eZHTTPTool::instance();
    }

    /**
     * Main function called by eZPublish kernel, using event listeners.
     * It is a wrapper for redirect public member method
     *
     * @param eZURI $uri
     * @return void
     */
    public static function redirectListener($uri) {
        $instance = new self;
        $instance->redirect($uri);
    }

    /**
     * Makes geo redirect if necessary
     *
     * @param eZURI $uri
     * @return void
     */
    public function redirect($uri) {
        // If user client does not support cookies, stop here
        if (!eZSession::userHasSessionCookie()) {
            return null;
        }

        // If current user country code is set in the cookies, we are not doing anything
        if (isset($_COOKIE[self::$countryCodeCookieVar])) {
            return null;
        }

        if ($this->http->hasVariable('disable_geo_redirect')) {
            return null;
        }

        $sa = eZSiteAccess::current();
        if ($this->isExcludeSiteaccesses($sa['name'])) {
            return null;
        }

        if ($this->isExcludeHTTPMethod(eZSys::serverVariable('REQUEST_METHOD', true))) {
            return null;
        }

        if ($this->isExcludePath($uri->originalURIString(true))) {
            return null;
        }

        if ($this->checkExcludeGETParameters()) {
            return null;
        }

        $geoData = $this->getGeoData();
        $this->setCountryCodeCookie($geoData['code']);

        $rule = $this->getMatchingRule($sa['name'], $geoData);
        if ($rule === null) {
            return null;
        }

        $this->doRedirectByRule($rule, $uri->originalURIString(true));
    }

    /**
     * Return current user Geo data, using GeoIP API
     *
     * @return array
     */
    protected function getGeoData() {
        include_once('extension/cc_georedirect/lib/geoip/geoip.inc');

        $ip  = eZSys::serverVariable('REMOTE_ADDR', true);
        $xIp = eZSys::serverVariable('HTTP_X_FORWARDED_FOR', true);
        if ($xIp !== null) {
            $tmp = explode(',', $xIp);
            $ip  = $tmp[0];
        }
        if ($this->http->hasVariable('cc_test_ip')) {
            $ip = $this->http->variable('cc_test_ip');
        }

        $gi = geoip_open(self::$geoDatabasePath, GEOIP_STANDARD);
        return array(
            'code' => geoip_country_code_by_addr($gi, $ip),
            'name' => geoip_country_name_by_addr($gi, $ip)
        );
    }


    /**
     * Return current user Geo data, using GeoIP API
     *
     * @return array
     */
    public static function getCurrentGeoData() {
        $instance = new self;
        return $instance->getGeoData();
    }

    /**
     * Checks if geo redirected is disabled for specified siteaccess
     *
     * @param string $siteaccess
     * @return bool
     */
    protected function isExcludeSiteaccesses($siteaccess) {
        $excludeSiteaccesses = $this->ini->variable('General', 'ExcludeSiteaccesses');
        return in_array($siteaccess, $excludeSiteaccesses);
    }

    /**
     * Checks if geo redirected is disabled for specified HTTP method
     *
     * @param string $HTTPMethod
     * @return bool
     */
    protected function isExcludeHTTPMethod($HTTPMethod) {
        $excludeMethods = $this->ini->variable('General', 'ExcludeHTTPMethods');
        return in_array($HTTPMethod, $excludeMethods);
    }

    /**
     * Checks if geo redirected is disabled for specified URI
     *
     * @param string $uri
     * @return bool
     */
    protected function isExcludePath($uri) {
        $excludePaths = $this->ini->variable('General', 'ExcludePaths');
        foreach ($excludePaths as $path) {
            if (strpos($uri, $path) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if geo redirect is disabled for current GET parameters set
     *
     * @param string $uri
     * @return bool
     */
    protected function checkExcludeGETParameters() {
        $excludeParams = $this->ini->variable('General', 'ExcludeGETParams');
        foreach ($excludeParams as $param => $value) {
            if (isset($_GET[$param]) && $_GET[$param] == $value) {
                return true;
            }
        }

        return false;
    }

    /**
     * Stores in cookies current user country code
     *
     * @param string $code
     * @return bool
     */
    protected function setCountryCodeCookie($code) {
        $hostname   = eZSys::hostname();
        $tmp        = array_reverse(explode('.', eZSys::serverVariable('SERVER_NAME', true)));
        $cookieHost = '.' . $tmp[1] . '.' . $tmp[0];
        setcookie(self::$countryCodeCookieVar, $code, time() + 60*60*24*365, '/', $cookieHost);
    }

    /**
     * Finds matching geo redirect rule
     *
     * @param string $siteaccess
     * @param array $geoData
     * @return string|null
     */
    protected function getMatchingRule($siteaccess, $geoData) {
        $rules = $this->ini->variable('General', 'Rules');
        foreach ($rules as $rule) {
            $group = 'Rule_' . $rule;
            if ($this->ini->hasGroup($group) === false) {
                continue;
            }

            // Check rule siteaccesses
            $siteaccesses = array();
            if ($this->ini->hasVariable($group, 'SiteAccesses')) {
                $siteaccesses = $this->ini->variable($group, 'SiteAccesses');
            }
            if (count($siteaccesses) > 0 && in_array($siteaccess, $siteaccesses) === false) {
                continue;
            }

            // Check rule cookies
            $includeCookies = array();
            if ($this->ini->hasVariable($group, 'IncludeCookies')) {
                $includeCookies = $this->ini->variable($group, 'IncludeCookies');
            }
            foreach ($includeCookies as $cookie => $value) {
                if (isset($_COOKIE[$cookie]) === false || $_COOKIE[$cookie] != $value) {
                    continue 2;
                }
            }
            $excludeCookies = array();
            if ($this->ini->hasVariable($group, 'ExcludeCookies')) {
                $excludeCookies = $this->ini->variable($group, 'ExcludeCookies');
            }
            foreach ($excludeCookies as $cookie => $value) {
                if (isset($_COOKIE[$cookie]) && $_COOKIE[$cookie] == $value) {
                    continue 2;
                }
            }

            // Check exclude countries
            $excludeCodes = array();
            if ($this->ini->hasVariable($group, 'ExcludeCountryCodes')) {
                $excludeCodes = (array) $this->ini->variable($group, 'ExcludeCountryCodes');
            }
            if (count($excludeCodes) > 0 && in_array($geoData['code'], $excludeCodes) === false) {
                return $rule;
            }

            // Check geo data
            $names = array();
            if ($this->ini->hasVariable($group, 'CountryNames')) {
                $names = (array) $this->ini->variable($group, 'CountryNames');
            }
            $codes = array();
            if ($this->ini->hasVariable($group, 'CountryCodes')) {
                $codes = (array) $this->ini->variable($group, 'CountryCodes');
            }

            if (
                in_array($geoData['name'], $names)
                || in_array($geoData['code'], $codes)
            ) {
                return $rule;
            }
        }

        return null;
    }

    /**
     * Redirects using specified rule
     *
     * @param string $rule
     * @param array $uri
     * @return void
     */
    protected function doRedirectByRule($rule, $uri) {
        $originalUri = $uri;
        $group = 'Rule_' . $rule;

        $prefix = null;
        if ($this->ini->hasVariable($group, 'RedirectPrefix')) {
            $prefix = $this->ini->variable($group, 'RedirectPrefix');
        }

        $host = null;
        if ($this->ini->hasVariable($group, 'RedirectHost')) {
            $host = $this->ini->variable($group, 'RedirectHost');
        }

        if ($prefix === null && $host === null) {
            return null;
        }

        if ($prefix !== null) {
            $uri = $prefix . $uri;
        }

        // Avoid infinite redirect loops
        if ($uri === $originalUri) {
            return null;
        }

        $query = eZSys::serverVariable('QUERY_STRING', true);
        if ($query !== null ) {
            $uri .= '?' . $query;
        }

        if ($host === null) {
            $host = eZSys::serverVariable('SERVER_NAME', true);
        }

        header('Location: ' . eZSys::serverProtocol() . '://' . $host . $uri);
        eZExecution::cleanExit();
    }
}
